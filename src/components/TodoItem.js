import React, { Component } from 'react';
import PropTypes from 'prop-types';

export default class TodoItem extends Component {
    getStyle = () => {
        return {
            textDecoration: this.props.todo.completed ? 'line-through' : 'none',
            color: this.props.todo.completed ? 'red' : 'gray'
        }
    }
    
    render() {
        const { id, title } = this.props.todo;
        return (
            <div className="todoItem" style={ this.getStyle() }>
                <p> 
                    <input type="checkbox" onChange={this.props.markComplete.bind(this, id)} />
                    {title}
                    <button onClick={this.props.deleteTodo.bind(this, id)} style={btnStyle} >Delete</button>
                </p>
            </div>
        )
    }
}
    
    
const btnStyle = {
    backgroundColor: 'red',
    color: 'white',
    fontWeight: 'bold',
    fontSize: '12px',
    border: 'none',
    padding: '5px 10px',
    borderRadius: '10%',
    cursor: 'pointer',
    float: 'right'
}

// PropTypes
TodoItem.propTypes = {
    todo: PropTypes.object.isRequired,
    markComplete: PropTypes.func.isRequired,
    deleteTodo: PropTypes.func.isRequired,
}
    